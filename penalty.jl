function logbarrier(x, δ)
    m = 1e8
    if x <= -δ
        m
    elseif x < 0.0
        min(m, -(x)^2 * log((x + δ) / δ))
    else
        0
    end
end
function dlogbarrier(x, δ)
    ee = δ^4 - δ
    f(x) = (x) * (-2 * log((x + δ) / δ) - (x) / (x + δ))
    m = f(ee)
    if x <= -δ
        m
    elseif x < 0.0
        max(m, f(x))
    else
        0
    end
end
function ddlogbarrier(x, δ)
    ee = δ^4 - δ
    f(x) = (δ / (x + δ))^2 + 2 * δ / (x + δ) - 2 * log((x + δ) / δ) - 3
    m = f(ee)
    if x <= -δ
        m
    elseif x < 0.0
        min(m, f(x))
    else
        0
    end
end
function barrier(x, δ)
    if x < δ
        -((x-δ)^3) / δ
    else
        0
    end
end
function dbarrier(x, δ)
    if x < δ
        -(3 / δ) * (x-δ)^2
    else
        0
    end
end
dbarrier_active(x, δ) = -(3 / δ) * (x - δ)^2
dbarrier_inactive(x, δ) = 0

function ddbarrier(x, δ)
    if x < δ
        -6 * (x-δ) / δ
    else
        0
    end
end
ddbarrier_active(x, δ) = -6 * (x - δ) / δ
ddbarrier_inactive(x, δ) = 0