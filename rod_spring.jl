# Define a RodSpring data structure for simulation and analysis.
Base.@kwdef mutable struct RodSpring{T}
    h::T = 0.025
    p::Array{T,1} = [0.0, 0.0]
    q::Array{T,1} = [1.0, -0.99]
    q_prev::Array{T,1} = [1.0, -1.0]
    v::Array{T,1} = [0.0, 0.0]
    v_prev::Array{T,1} = [0.0, 0.0]
    l0::T = sqrt(2.0)
    vs::T = -0.5
    m::T = 1.0
    k::T = 1.0
    µ::T = 0.1
    g::T = 9.81
    α::T = 1.0
    β::T = 0.0
    eps::T = 0.01
    delta::T = 0.01
    kappa::T = 1e7
end

# The copy function allows this struct to be duplicated.
Base.copy(r::RodSpring) = RodSpring(
    r.h,
    [r.p[1], r.p[2]],
    [r.q[1], r.q[2]],
    [r.q_prev[1], r.q_prev[2]],
    [r.v[1], r.v[2]],
    [r.v_prev[1], r.v_prev[2]],
    r.l0, r.vs, r.m, r.k, r.μ, r.g, r.α, r.β, r.eps, r.delta, r.kappa)