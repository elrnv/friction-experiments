# Advance simulation
function advance(rs, integrators, pos, vel)
    iterations = []
    total_ls = []
    for (step, r) in zip(integrators, rs)
        i,ls = step(r)
        push!(iterations, i)
        push!(total_ls, ls)
    end
    for (r, outp, outv) in zip(rs, pos, vel)
        push!(outp, r.q)
        push!(outv, r.v)
    end
    iterations, total_ls
end

# Prepare data for simulation
function prep(r::RodSpring, integrators; T=30.0)
    h = r.h
    frames = Int(round(T / h))
    xs = (0:(frames)) .* h
    rs = [Base.copy(r) for _ in integrators]
    pos = [[r.q] for _ in integrators]
    vel = [[r.v] for _ in integrators]
    h, frames, xs, rs, pos, vel
end

# Compute simulation output (no animation or plots)
function eval(r::RodSpring, integrators; T=30.0)
    h, frames, xs, rs, pos, vel = prep(r, integrators; T=T)
    total_iter = [0 for _ in integrators];
    total_ls = [0 for _ in integrators];
    for i in 1:frames
        iter, ls = advance(rs, integrators, pos, vel)
        for (i, c) in enumerate(iter)
            total_iter[i] += c
        end
        for (i, c) in enumerate(ls)
            total_ls[i] += c
        end
    end
    
    xs, pos, vel, total_iter, total_ls
end

# This function creates a conveyor belt plot with the given horizontal offset
function make_conveyor(off, yoff = -1.0)
    off = -(-off % 0.1)
    conveyor_x = collect(Iterators.flatten([x, x + 0.04, x] for x in -3:0.1:2))
    conveyor_y = collect(Iterators.flatten([0, -0.04, 0] .+ yoff for i in -3:0.1:2))
    for (i, x) in enumerate(conveyor_x)
        conveyor_x[i] = x + off
    end
    conveyor_x, conveyor_y
end;

# Plot multiple integrators into one plot and animation sequence.
function sim(r::RodSpring, integrators, labels::Array{String,1}, colors, outfile::String; ground_truth=nothing, T=30.0, fps=24, xlims=[-1.5,1.0], ylims=[-1.0, 0.0])
    @assert size(integrators) == size(labels)
    @assert size(integrators) == size(colors)
    h, frames, xs, rs, pos, vel = prep(r, integrators; T=T)
    n = length(integrators)
    progress = Progress(frames)
    
    substeps = Int(ceil(1.0 / (h * fps)))
    println("substeps = $substeps")

    anim = @animate for i = 1:frames
        t = (i - 1) * h
        plt = plot(legend=:topleft, xlims=xlims, ylims=[-1.1, 0.1], aspect_ratio=1)
        for (r, name, color, k) in zip(rs, labels, colors, 0:(n - 1))
            plot!(plt, [r.p[1], r.q[1]], [r.p[2], r.q[2]], label=name, color=color) 
        end
        for (r, name, k) in zip(rs, labels, 0:(n - 1))
            scatter!(plt, [r.p[1], r.q[1]], [r.p[2], r.q[2]], label=nothing, color=:gray)
        end
        if ground_truth != nothing
            q, v = ground_truth(t)
            plot!(plt, [r.p[1], q[1]], [r.p[2], q[2]], label="GT", color=:gray) 
            scatter!(plt, [r.p[1], q[1]], [r.p[2], q[2]], label=nothing, color=:gray)
        end

        conveyor_x, conveyor_y = make_conveyor(t * r.vs, -1.0)
        plot!(plt, conveyor_x, conveyor_y, label=nothing)
        posplt = plot(legend=:none, xlims=[0,T], ylims=ylims)
        
        velplt = plot(legend=:none, xlims=[0,T], ylims=[r.vs - 0.1, -r.vs + 0.1])
        if ground_truth != nothing
            gtts = 0:0.001:T
            out = ground_truth.(gtts)
            plot!(posplt, gtts, [x[1][1] for x in out], label="GT", color=:gray)
            plot!(velplt, gtts, [x[2][1] for x in out], label="GT", color=:gray)
        end

        for (outp, outv, name, color) in zip(pos, vel, labels, colors)
            plot!(posplt, (0:(i - 1)) .* h, [x[1] for x in outp], label=name, color=color)
            plot!(velplt, (0:(i - 1)) .* h, [x[1] for x in outv], label=name, color=color)
        end
        plot(plt, posplt, velplt, layout=grid(3, 1, heights=[0.6,0.2,0.2]), size=(800, 600))
        
        advance(rs, integrators, pos, vel)
        next!(progress)
    end every substeps
    
    mp4(anim, outfile, fps=fps), xs, pos, vel
end;