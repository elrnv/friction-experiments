

# Plot the position or velocity of the entire animation
# xs are the times
# data is an array of position or velocity data (each element is an array of ys against given xs)
function plotsim(xs, data, labels, colors; xlims, ylims, size, legend=:best, title="", ground_truth = nothing)
    @assert length(xs) == length(data[1])
    xdata = [[v[1] for v in elem] for elem in data]
    plt = plot(xlims=xlims, ylims=ylims, size=size)
    if ground_truth != nothing
        plot!(plt, xs, [y[1] for y in ground_truth.(xs)], label="GT", color=:gray, linewidth=5)
    end
    for (name, ys, c) in zip(labels, xdata, colors)
        plot!(plt, xs, ys, label=name, color=c, linewidth=2, legend=legend, title=title)
    end
    plt
end;
function plotpos(xs, data, labels, colors, vs; xlims, ylims, conveyor_freq=2.8, conveyor_off=0.0, ground_truth = nothing)
    plt = if ground_truth != nothing
        posgt(t) = ground_truth(t)[1]
        plotsim(xs, data, labels, colors, xlims=xlims, ylims=ylims, size=(800, 400), ground_truth=posgt)
    else
        plotsim(xs, data, labels, colors, xlims=xlims, ylims=ylims, size=(800, 400))
    end
        
    for i in 0:6
        plot!(plt, xs .+ i*conveyor_freq .+ conveyor_off, xs*vs .+ 1, label=nothing, color=:gray)
    end
    plt
end
function plotvel(xs, data, labels, colors, vs, eps; xlims, ylims = [vs, -vs], legend=:best, title="", ground_truth = nothing, conveyor=true)
    plt = if ground_truth != nothing
        velgt(t) = ground_truth(t)[2]
        plotsim(xs, data, labels, colors, xlims=xlims, ylims=ylims, size=(800, 400), title=title, legend=legend, ground_truth=velgt)
    else
        plotsim(xs, data, labels, colors, xlims=xlims, ylims=ylims, size=(800, 400), title=title, legend=legend)
    end
    if conveyor
        plot!(plt, xs, [vs for _ in xs], label="conveyor", color=:grey)
        plot!(plt, xs, [vs+eps for _ in xs], label="conveyor + eps", color=:lightgrey)
    end
    plt
end;
