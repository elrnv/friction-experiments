K(r::RodSpring, q) = r.k * ((norm(q) - r.l0) / norm(q)) * I(2)
dKq(r::RodSpring, q) = (r.k / norm(q)) * ((r.l0 / (q' * q)) * q * q') + K(r, q)
dKdqv(r::RodSpring, v, q) = r.k*r.l0*v*q'/norm(q)^3
M(r::RodSpring) = r.m * I(2)
D(r::RodSpring, q) = r.α * M(r) + r.β * K(r, q)
dDdqv(r::RodSpring, v, q) = r.β * dKdqv(r, v, q)
function A(r::RodSpring, q)
    [Av(r, q); [I(2) zeros(2, 2)]]
end
function Av(r::RodSpring, q)
    Minv = inv(M(r))
    [-Minv * D(r, q) -Minv * K(r, q)]
end
dAvdqu(r::RodSpring, v, q) = -M(r)\(dDdqv(r, v, q) + dKq(r,q))
dAvdvu(r::RodSpring, v, q) = -M(r)\D(r, q)
dAvduu(r::RodSpring, v, q) = [dAvdvu(r, v, q) dAvdqu(r, v, q)]
var(r::RodSpring) = [r.v; r.q]
var_prev(r::RodSpring) = [r.v_prev; r.q_prev]
d(q) = q[2] + 1.0
## AKA dd/dq
n(q) = [0; 1]
λ_active(r::RodSpring, q) = -r.kappa * dbarrier_active(d(q), r.delta)
λ_inactive(r::RodSpring, q) = -r.kappa * dbarrier_inactive(d(q), r.delta)
λ(r::RodSpring, q) = -r.kappa * dbarrier(d(q), r.delta)
dλ_active(r::RodSpring, q) = -r.kappa * ddbarrier_active(d(q), r.delta) * n(q)
dλ_inactive(r::RodSpring, q) = -r.kappa * ddbarrier_inactive(d(q), r.delta) * n(q)
dλ(r::RodSpring, q) = -r.kappa * ddbarrier(d(q), r.delta) * n(q)
fc(r::RodSpring, q) = λ(r, q) * n(q)
fc_custom(r::RodSpring, q, λ) = λ(r, q) * n(q)
dfc_active(r::RodSpring, q) = dλ_active(r, q) * n(q)'
dfc_inactive(r::RodSpring, q) = dλ_inactive(r, q) * n(q)'
dfc(r::RodSpring, q) = dλ(r, q) * n(q)'
dfc_custom(r::RodSpring, q, dλ) = dλ(r, q) * n(q)'
function Fv_custom(r::RodSpring, v, q, eta, λ)
    f_c = fc_custom(r, q, λ)
    [0; -r.g] + M(r) \ (f_c - r.μ * norm(f_c) * eta([v[1] - r.vs, 0], r.eps))
end
function Fv(r::RodSpring, v, q, eta)
    Fv_custom(r, v, q, eta, λ)
end
function dFvdv_custom(r::RodSpring, v, q, dqdv, eta, deta, λ, dλ)
    dfc_ = dfc_custom(r, q, dλ) * dqdv
    eta_ = eta([v[1] - r.vs, 0.0], r.eps)
    deta_ = deta([v[1] - r.vs, 0.0], r.eps) * [1 0; 0 0]
    dv = -r.μ * (eta_ * dλ(r, q)' * dqdv + λ(r, q) * deta_') + dfc_
    df = M(r) \ dv
end
function dFvdv(r::RodSpring, v, q, dqdv, eta, deta)
    dFvdv_custom(r, v, q, dqdv, eta, deta, λ, dλ)
end
F(r::RodSpring, u, eta) = [Fv(r, u[1:2], u[3:4], eta); 0; 0]
F1(r, u) = F(r, u, eta1)
F2(r, u) = F(r, u, eta2)
# v part of f
fv(r::RodSpring, v, q, eta) = Av(r, q) * [v; q] + Fv(r, v, q, eta)
fv_custom(r::RodSpring, v, q, eta, λ) = Av(r, q) * [v; q] + Fv_custom(r, v, q, eta, λ)
# derivative of v part of f wrt v
function dfvdv_custom(r::RodSpring, v, q, dqdv, eta, deta, λ, dλ)
    M(r) \ (-D(r, q) - dKq(r, q) * dqdv) + dFvdv_custom(r, v, q, dqdv, eta, deta, λ, dλ)
end
function dfvdv(r::RodSpring, v, q, dqdv, eta, deta)
    dfvdv_custom(r, v, q, dqdv, eta, deta, λ, dλ)
end