# B_eps

function dBeps(x, eps)
    eps / (abs(x) + eps)^2
end;

# Approximation to x/|x|
function Beps(x, eps)
    x / (norm(x) + eps)
end;

# Version of Beps divided by x (for avoiding divide by zero)
function Beps_div(x, eps)
    1 / (abs(x) + eps)
end;

# B1

function dB1(x, eps)
    if x < -eps || x > eps
        0.0
    else
        1.0 / eps
    end
end;
function B1(x, eps)
    if x < -eps
        -1
    elseif x > eps
        1
    else
        x / eps
    end
end;
# Version of B1 divided by x to avoid division by zero elsewhere.
function B1_div(x, eps)
    if x < -eps
        -1 / x
    elseif x > eps
        1 / x
    else
        1 / eps
    end
end;
function dB1_div(x, eps)
    @assert x >= 0
    if x > eps
        -1 / (x*x)
    else
        0
    end
end;

function b1(x, eps::Float64)
    if x < -eps
        -x - eps / 2
    elseif x > eps
        x - eps / 2
    else
        x * x / (2 * eps)
    end
end;

# B2

function dB2(x, eps)
    if x < -eps || x > eps
        0.0
    elseif x < 0
        2 / eps + 2 * x / (eps * eps)
    else
        2 / eps - 2 * x / (eps * eps)
    end
end

function B2(x, eps)
    if x < -eps
        -1
    elseif x > eps
        1
    elseif x < 0
        2 * x / eps + x * x / (eps * eps)
    else
        2 * x / eps - x * x / (eps * eps)
    end
end
dB2_slide(x, eps) = 0
dB2_stick(x, eps) = 2 / eps - 2 * x / (eps * eps)
# Version of B2 divided by x to avoid division by zero elsewhere.
function B2_div(x, eps)
    if x < -eps
        -1 / x
    elseif x > eps
        1 / x
    elseif x < 0
        2 / eps + x / (eps * eps)
    else
        2 / eps - x / (eps * eps)
    end
end
function dB2_div(x, eps)
    @assert x >= 0
    if x < eps
        -1/(eps*eps)
    else
        -1/(x*x)
    end
end

# Assume positive x
B2_slide(x,eps) = 1
B2_stick(x,eps) = 2 / eps - x / (eps * eps)

function b2(x, eps::Float64)
    if x < -eps
        -x - eps / 3
    elseif x > eps
        x - eps / 3
    elseif x < 0
        x * x / eps + x * x * x / (3 * eps * eps)
    else
        x * x / eps - x * x * x / (3 * eps * eps)
    end
end;

# B3

function B3(x, eps)
    if x < -eps
        -1
    elseif x > eps
        1
    else
        3 * x / (2 * eps) - x * x * x / (2 * eps * eps * eps)
    end
end

# eta

function eta(u, eps, B_div)
    nu = norm(u)
    #if nu > 0.0
        B_div(nu, eps) * u
    #else
    #    [0,0]
    #end
end
eta2_stick(u, eps) = B2_stick(norm(u), eps) * u
eta2_slide(u, eps) = u / norm(u)
eta1(u, eps) = eta(u, eps, B1_div)
eta2(u, eps) = eta(u, eps, B2_div)
eta_eps(u, eps) = u / (norm(u) + eps)

# deta

function deta(u, eps, B_div, dB_div)
    nu = norm(u)
    out = B_div(nu, eps) * I(2)
    if nu > 0.0
        out += (dB_div(nu, eps)/nu) * u * u'
    end
    out
end

function deta2_stick(u, eps)
    nu = norm(u)
    uu = u / nu
    dB2_stick(nu, eps) * uu * uu' + (B2_stick(nu, eps)) * (I(2) - uu * uu')
end

function deta2_slide(u, eps)
    nu = norm(u)
    uu = u / nu
    dB2_slide(nu, eps) * uu * uu' + (B2_slide(nu, eps)) * (I(2) - uu * uu')
end

deta1(u, eps) = deta(u, eps, B1_div, dB1_div)
deta2(u, eps) = deta(u, eps, B2_div, dB2_div)
function deta_eps(u, eps)
    nu = norm(u)
    if nu > 0.0
        [1 / (nu + eps) - u[1] * u[1] / (nu * (nu + eps)^2) -u[1] * u[2] / (nu * (nu + eps)^2);
        -u[1] * u[2] / (nu * (nu + eps)^2) 1 / (nu + eps) - u[2] * u[2] / (nu * (nu + eps)^2)]
    else
        [1 / eps 0.0;
        0.0 1 / eps]
    end
end